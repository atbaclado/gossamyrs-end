'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn('lists', 'type', {
      type: Sequelize.ENUM('manga', 'webtoon', 'novel'),
      allowNull: false
    });
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('lists', 'type');
  }
};
